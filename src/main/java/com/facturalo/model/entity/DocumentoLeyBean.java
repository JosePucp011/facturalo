/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.facturalo.model.entity;

import lombok.Data;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;

/**
 *
 * @author LUISINHO
 */

@Data
@Entity
@Table( name = "leyenda")
public class DocumentoLeyBean {

    private @Id @GeneratedValue Integer id_leyenda;
    private Integer docu_codigo;
    private String leyenda_codigo;
    private String leyenda_texto;
}