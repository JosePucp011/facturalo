/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.facturalo.model.entity;

import lombok.Data;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;

/**
 *
 * @author LUISINHO
 */

@Data
@Entity
@Table( name = "detalle")
public class DocumentoDetBean {

    private @Id @GeneratedValue Integer iddetalle;
    private  Integer docu_codigo;


    //private String item_moneda;
    private String item_orden;
    private String item_unidad;
    private String item_cantidad;
    private String item_codproducto;
    private String item_descripcion;
    private String item_afectacion;
    private String item_pventa;
    private String item_pventa_nohonerosa;
    private String item_to_subtotal;
    private String item_to_igv;


 
}
