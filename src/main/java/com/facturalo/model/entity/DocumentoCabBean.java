/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.facturalo.model.entity;

import lombok.Data;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;

/**
 *
 * @author LUISINHO
 */

@Data
@Entity
@Table( name = "cabecera")
public class DocumentoCabBean {

    private @Id @GeneratedValue Integer docu_codigo;
    private Integer id_externo;
    private String empr_razonsocial;
    private String empr_ubigeo;
    private String empr_nombrecomercial;
    private String empr_direccion;
    private String empr_provincia;
    private String empr_departamento;
    private String empr_distrito;
    private String empr_pais;
    private String empr_nroruc;
    private String empr_tipodoc;
    private String clie_numero;
    private String clie_tipodoc;
    private String clie_nombre;
    private String docu_fecha; 
    private String docu_hora; 
    private String docu_tipodocumento;
    private String docu_numero;
    private String docu_moneda;
    private Double docu_gravada;
    private String docu_inafecta;
    private String docu_exonerada;
    private String docu_gratuita;
    private String docu_descuento;
    private String docu_subtotal;
    private String docu_total;
    private String docu_igv;
    private String tasa_igv;
    private String docu_isc;
    private String tasa_isc;
    private String docu_otrostributos;
    private String tasa_otrostributos;
    private String docu_otroscargos;
    private String docu_enviaws;
    private String clie_correo_cpe1;
    private String clie_correo_cpe2;





    
}
