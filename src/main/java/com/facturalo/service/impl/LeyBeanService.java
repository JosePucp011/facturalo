package com.facturalo.service.impl;

import com.facturalo.model.entity.DocumentoLeyBean;
import com.facturalo.service.dao.LeyBeanDAO;
import com.facturalo.service.interfac.ILeyBeanRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class LeyBeanService implements ILeyBeanRepository {

    @Autowired
    private LeyBeanDAO leyBeanDAO;

    public List<DocumentoLeyBean> cargarDetDocElectronicoLeyenda(Integer pdocu_codigo) {
        return leyBeanDAO.findListLey(pdocu_codigo);//arreglar docucodigo ByDocuCodigo
    }
}
