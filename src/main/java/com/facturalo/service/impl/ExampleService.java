package com.facturalo.service.impl;

import com.facturalo.model.entity.Example;
import com.facturalo.service.dao.ExampleDAO;
import com.facturalo.service.interfac.IExample;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class ExampleService implements IExample {

    @Autowired
    private ExampleDAO exampleDAO;

    @Override
    public List<Example> findAllExample() {
        return exampleDAO.findAll();
    }

}
