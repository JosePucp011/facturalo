package com.facturalo.service.impl;

import com.facturalo.model.entity.DocumentoDetBean;
import com.facturalo.service.dao.DetBeanDAO;
import com.facturalo.service.interfac.IDetBeanRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class DetBeanService implements IDetBeanRepository {

    @Autowired
    private DetBeanDAO detBeanDAO;

    @Override
    public List<DocumentoDetBean> cargarDetDocElectronico(Integer val) {
        return detBeanDAO.findListDet(val);
    }
}
