package com.facturalo.service.impl;

import com.facturalo.model.entity.DocumentoCabBean;
import com.facturalo.service.dao.CabBeanDAO;
import com.facturalo.service.interfac.ICabBeanRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;


@Service
public class CabBeanService implements ICabBeanRepository {

    @Autowired
    private CabBeanDAO cabBeanDAO;

    @Override
    public DocumentoCabBean cargarDocElectronico(Integer pdocu_codigo) {
        return cabBeanDAO.findById(pdocu_codigo).orElse(null);
    }

    @Override
    public DocumentoCabBean pendienteDocElectronico() {
        return cabBeanDAO.findPendientes();
    }

    @Override
    public DocumentoCabBean noPendienteDocElectronico() {
        return cabBeanDAO.findNoPendientes();
    }
}
