package com.facturalo.service.dao;

import com.facturalo.model.entity.DocumentoDetBean;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import java.util.List;

public interface DetBeanDAO extends JpaRepository<DocumentoDetBean, Integer> {

    @Query(value = "select * from detalle where docu_codigo = :val", nativeQuery = true)
    List<DocumentoDetBean> findListDet(@Param("val") Integer val);

    //@Query(value = "SELECT * FROM consumer WHERE lastname = :lastName  LIMIT :maxRecord", nativeQuery = true)
    //StickerProduct findWalletProduct(@Param("lastName") String lastName, @Param("maxRecord") Long maxRecord);
}

