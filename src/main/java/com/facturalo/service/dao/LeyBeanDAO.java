package com.facturalo.service.dao;

import com.facturalo.model.entity.DocumentoDetBean;
import com.facturalo.model.entity.DocumentoLeyBean;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import java.util.List;

public interface LeyBeanDAO extends JpaRepository<DocumentoLeyBean, Integer> {

    @Query(value = "select * from leyenda where docu_codigo = :val", nativeQuery = true)
    List<DocumentoLeyBean> findListLey(@Param("val") Integer val);
}

