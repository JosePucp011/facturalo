package com.facturalo.service.dao;

import com.facturalo.model.entity.DocumentoCabBean;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

public interface CabBeanDAO extends JpaRepository<DocumentoCabBean, Integer> {

    @Query(value = "SELECT * FROM cabecera c where c.docu_proce_status = 'N' order by docu_codigo LIMIT 1", nativeQuery = true)
    public DocumentoCabBean findPendientes();

    @Query(value = "SELECT * FROM cabecera WHERE  docu_proce_status in ('B','P','E','X') and docu_proce_fecha <=  DATE_SUB(NOW(), INTERVAL 10 MINUTE)  order by docu_codigo LIMIT 1 ", nativeQuery = true)
    public DocumentoCabBean findNoPendientes();

}
