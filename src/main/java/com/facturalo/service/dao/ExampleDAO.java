package com.facturalo.service.dao;
import com.facturalo.model.entity.Example;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface ExampleDAO extends JpaRepository<Example, Integer> {


}
