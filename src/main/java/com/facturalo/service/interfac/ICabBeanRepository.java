package com.facturalo.service.interfac;

import com.facturalo.model.entity.DocumentoCabBean;

public interface ICabBeanRepository {

    public DocumentoCabBean cargarDocElectronico(Integer pdocu_codigo);
    public DocumentoCabBean pendienteDocElectronico();
    public DocumentoCabBean noPendienteDocElectronico();

}
