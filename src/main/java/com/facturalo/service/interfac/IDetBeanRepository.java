package com.facturalo.service.interfac;


import com.facturalo.model.entity.DocumentoDetBean;

import java.util.List;

public interface IDetBeanRepository {

    public List<DocumentoDetBean> cargarDetDocElectronico(Integer pdocu_codigo);
}
