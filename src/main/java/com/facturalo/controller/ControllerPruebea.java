package com.facturalo.controller;

import com.facturalo.model.entity.DocumentoCabBean;
import com.facturalo.model.entity.DocumentoDetBean;
import com.facturalo.model.entity.DocumentoLeyBean;
import com.facturalo.model.entity.Example;
import com.facturalo.service.impl.CabBeanService;
import com.facturalo.service.impl.DetBeanService;
import com.facturalo.service.impl.ExampleService;
import com.facturalo.service.impl.LeyBeanService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
public class ControllerPruebea {

    @Autowired
    private ExampleService exampleService;

    @Autowired
    private CabBeanService cabBeanService;

    @Autowired
    private DetBeanService detBeanService;

    @Autowired
    private LeyBeanService leyBeanService;

    @GetMapping("/saludo")
    public String saludo() {
        return "Este es un saludo de prueba";
    }


    @RequestMapping(value="/example", method= RequestMethod.GET, produces=MediaType.APPLICATION_JSON_VALUE)
    public List<Example> example() {
        return exampleService.findAllExample();
    }

    @RequestMapping(value="/cabecera", method= RequestMethod.GET, produces=MediaType.APPLICATION_JSON_VALUE)
    public DocumentoCabBean cabecera() {
        return cabBeanService.pendienteDocElectronico();
    }

    @RequestMapping(value="/detalle/{id}", method= RequestMethod.GET, produces=MediaType.APPLICATION_JSON_VALUE)
    public List<DocumentoDetBean> detalle(@PathVariable Integer id) {
        return detBeanService.cargarDetDocElectronico(id);
    }

    @RequestMapping(value="/leyenda/{id}", method= RequestMethod.GET, produces=MediaType.APPLICATION_JSON_VALUE)
    public List<DocumentoLeyBean> leyenda(@PathVariable Integer id) {
        return leyBeanService.cargarDetDocElectronicoLeyenda(id);
    }



}
